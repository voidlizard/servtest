module App.Types
    ( Env(..), asks
    , AppT, runAppT
    , AppError(..), appError, servantError
    , logDebugN
    , redis
    , PG.pgQuery, PG.pgExecute
    ) where

import           Control.Exception
import           Control.Monad.Logger
import           Control.Monad.Reader      (ReaderT (runReaderT), asks)
import qualified Database.PostgreSQL.Query as PG
import qualified Database.Redis            as R

import           Data.Aeson
import           Data.Pool
import           System.Log.FastLogger

import           API.V1.Types
import           Import.NoApp

data Env = Env
    { getUsers  :: IO [User]
    , loggerSet :: LoggerSet
    , pgPool    :: Pool PG.Connection
    , redisPool :: R.Connection
    }

type AppT = ReaderT Env IO

runAppT :: Env -> AppT a -> IO a
runAppT env app = runReaderT app env

data AppError =
    AppError Text Value
  | ServantError ServantErr
  | RedisError R.Reply
  | PostgresError String
  deriving (Show, Eq)

instance Exception AppError

appError :: ToJSON b
         => Text -> b -> AppT a
appError msg = liftIO . throwIO
             . AppError msg
             . toJSON

servantError :: ServantErr -> AppT a
servantError = liftIO . throwIO
             . ServantError

-- Services

instance {-# OVERLAPPING #-} MonadLogger AppT where
    monadLoggerLog loc src lvl msg = do
        ls <- asks loggerSet
        liftIO . pushLogStr ls
               . defaultLogStr loc src lvl
               $ toLogStr msg

instance {-# OVERLAPPING #-} PG.HasPostgres AppT where
    withPGConnection action = do
        pool <- asks pgPool
        withResource pool action

redis :: R.Redis (Either R.Reply a) -> AppT a
redis action = do
    pool <- asks redisPool
    result <- liftIO $ R.runRedis pool action
    case result of
        Right x -> pure x
        Left e -> liftIO . throwIO $ RedisError e
