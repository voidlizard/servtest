module API.V1.Handlers where

import Import
import API.V1.Types

handlers :: ServerT API AppT
handlers =
         userList
    :<|> userDetails

fetchUsers :: AppT [User]
fetchUsers = asks getUsers >>= liftIO

userList :: AppT [User]
userList = fetchUsers

userDetails :: Int -> AppT User
userDetails 1337 = error "crash and burn"
userDetails uid = do
    users <- fetchUsers
    case [u | u <- users, userId u == uid ] of
        [u] -> pure u
        [] -> servantError err404
        _ -> appError "id not unique" uid
