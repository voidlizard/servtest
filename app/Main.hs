module Main where

import App.Server (startApp)

main :: IO ()
main = startApp
