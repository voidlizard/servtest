Here is a starter servant [project] extended to a full-blown yesod-ish application.

[project]: https://github.com/commercialhaskell/stack-templates/blob/master/servant.hsfiles

## Features

Remove bits you don't need before hitting `build`.

* Standalone HTTP2 (TLS-enabled) server
* Servant-typed API routes
    * Versioned API endpoints (`/api/v1` -> `API.V1.*`)
    * WebSocket application (`/ws/echo` -> `WS.Echo`)
    * Static files fallback (`/`)
* Base monad providing services to handlers:
    * Logging
    * Database (`postgresql-query`)
    * Cache (`hedis`)
    * Some data for starter API (`User`)
* Startup procedures
    * ~~Configuration loading~~ **TODO**
    * Service initialization and validation
* Tests scaffold with HSpec
* Docker image settings

## Layout

### Files

``` bash
.        # only build-related stuff
./src    # implementation code
./app    # main application binary
./test   # some things you may want to check before shipping
./etc    # runtime configuration files
./static # bits to serve as site root
```

### Modules

``` haskell
Import       -- For handlers
Import.NoApp -- For core types

App.Server   -- Application assembling and running
App.Types    -- Environment and service helpers

API.*        -- Servant components

WS.*         -- WebSocket components
```
