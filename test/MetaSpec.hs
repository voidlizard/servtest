module MetaSpec (spec) where

import Test.Hspec
import Test.QuickCheck

spec :: Spec
spec = describe "HSpec usage demo" $
    describe "Not very difficult after all" $ do
        it "follows the Descriptive And Meaning Phrases convention" $
            head [23..] `shouldBe` (23 :: Int)
        it "bridges multiple testing methods" $
            property $ \x xs -> head (x:xs) == (x :: Int)
